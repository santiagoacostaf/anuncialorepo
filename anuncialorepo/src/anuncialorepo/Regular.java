package anuncialorepo;
import java.util.*;


public class Regular extends User {
	//Campos de la clase
    private int postViews;
    private int strikesNumber;
    private int starsNumber;
    private int List<Post> post = new ArrayList<>();
    //Metodos setters y getters de la clase
    public int getPostViews() {
        return postViews;
    }

    public void setPostViews(int postViews) {
        this.postViews = postViews;
    }

    public int getStrikesNumber() {
        return strikesNumber;
    }

    public void setStrikesNumber(int strikesNumber) {
        this.strikesNumber = strikesNumber;
    }

    public int getStarsNumber() {
        return starsNumber;
    }

    public void setStarsNumber(int starsNumber) {
        this.starsNumber = starsNumber;
    }
    
    public List<Post> getPost() {
        return post;
    }

    public void setPost(List<Post> post) {
        this.post = post;
    }
    
    
}
