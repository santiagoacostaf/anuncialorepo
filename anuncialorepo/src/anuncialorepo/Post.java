package anuncialorepo; //Change
import java.util.*;


public class Post {
//Variables de la clase
    private String topic;
    private Date dateCreated;
    private Date lastUpdate;
    private boolean isAllowed;
    private List<File> file = new ArrayList<>();
//Estos son los m�todos de la clase
    private void comment(String string){
    	throw new UnsupportedOperationException("Funcionalidad comentar todav�a no implementada");
    }
    
    private void rate(){
    	throw new UnsupportedOperationException("Funcionalidad calificar todav�a no implementada");
    }
    
    private void share(){
    	throw new UnsupportedOperationException("Funcionalidad compartir todav�a no implementada");
    }

    public String getTopic() {
        return topic;
    }
//Metodos setters y getters del proyecto, se reciben y retornan los campos de la clase
    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isIsAllowed() {
        return isAllowed;
    }

    public void setIsAllowed(boolean isAllowed) {
        this.isAllowed = isAllowed;
    }
    
    public List<File> getFile() {
        return file;
    }

    public void setFile(List<File> file) {
        this.file = file;
    }
    
}
