package anuncialorepo; //dolopezo committ

public class File{
	//Variables de la clase
    private String fileType;
    private byte[] content;
    private double size;
    //Metodos de la clase
    private void download(){
    	throw new UnsupportedOperationException("Funcionalidad descargar todav�a no implementada");
    }
    
    private void share(){
    	throw new UnsupportedOperationException("Funcionalidad compartir todav�a no implementada");
    }

    public String getFileType() {
        return fileType;
    }
    //Metodos setters y getters de la clase
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
}
