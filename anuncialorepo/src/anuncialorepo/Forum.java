package anuncialorepo;
import java.util.*;


public class Forum {
	//Estos son los campos de la clase
    private String name;
    private Date dateCreated;
    private String category;
    private List<Post> post = new ArrayList<>();
    //Metodos setters y getters de la clase
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    public List<Post> getPost() {
        return post;
    }

    public void setFile(List<Post> post) {
        this.post = post;
    }
    
}
