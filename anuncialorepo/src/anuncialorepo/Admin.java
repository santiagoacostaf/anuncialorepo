package anuncialorepo; //Modifcacion
import java.util.*;

public class Admin extends User{
    //Campos de las clases
    private int level;
    private double rating;
    private List<Forum> forum = new ArrayList<>();
    // Metodos setters y getters de las clases
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
    
    public List<Forum> getForum(){
    	return forum;
    }
    
    public void setForum(List<Forum> forum){
    	this.forum=forum;
    }
    
}
